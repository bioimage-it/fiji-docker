macro threshold{

	// parse args
	args = parseArgs();

	// open the data
	print(args[0])
	open(args[0]);

	// Threshold
	setAutoThreshold(args[1] + " dark");
	print("autothreshold", args[1] + " dark")
	//setOption("BlackBackground", false);
	run("Convert to Mask");

	// save result image
	saveAs("TIFF", args[2]);

}

function parseArgs(){
	argsStr = getArgument()
	argsStr = substring(argsStr, 1, lengthOf(argsStr)); // remove first char
	argsStr = substring(argsStr, 0, lengthOf(argsStr)-1); // remove last char
	print(argsStr);
	args = split(argsStr, ",");
	for (i=0 ; i < args.length ; i++){
		print(args[i]);
	}
	return args;
}
