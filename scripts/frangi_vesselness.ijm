macro frangi_vesselness{

	// parse args
	args = parseArgs();

	// open the data
	open(args[0]);
	image = getTitle();

	dogauss = args[1]
	spacing1 = args[2]
	spacing2 = args[3]
	scale1 = args[4]
	scale2 = args[5]

	// run
	run("Frangi Vesselness", "input="+image+" dogauss="+dogauss+" spacingstring=["+spacing1+", "+spacing2+"] scalestring=["+scale1+", "+scale2+"]");

	// save result image
	saveAs("TIFF", args[6]);

}

function parseArgs(){
	argsStr = getArgument()
	argsStr = substring(argsStr, 1, lengthOf(argsStr)); // remove first char
	argsStr = substring(argsStr, 0, lengthOf(argsStr)-1); // remove last char
	print(argsStr);
	args = split(argsStr, ",");
	for (i=0 ; i < args.length ; i++){
		print(args[i]);
	}
	return args;
}
